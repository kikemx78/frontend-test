import * as React from 'react';
import { useState } from 'react';
import { CoverageModel, VehicleModelsEnum } from '../models/CoverageModel';
import Data from '../../data/coverage.json';
import logo from './../../images/logo.png';
import { ModelYearCell } from '../components/ModelYearCell';

export const Grid: React.FC = () => {

  const [state, setState] = useState<CoverageModel>({...Data[0]});

  const handleToggleCovered = (vehicleModel: VehicleModelsEnum, year: number): void => {
    const isYearCovered = state.coverage[vehicleModel].find(year_ => year === year_);
    // add/remove coverage from model->coverage array 
    const action = isYearCovered  ? 
    state.coverage[vehicleModel].filter(year_ => year_ !== year) :
    [...state.coverage[vehicleModel], year];
    setState({
      ...state,
      coverage: {
        ...state.coverage,
        [vehicleModel]: action
      }
    });
  };

  return (
    <div className="table-container">
      <div className="inner">
        
          <div className="vehicles-col">
            {/* Renders the logo */}
            <div className="row">
              <div className="cell logo vehicles" style={logoStyles}></div> 
            </div>
            {/* Renders side bar with vehicle model list */}
            {
              state['vehicle-models']
                .map((vehicleModel, i) =>
                  <div key={i} className="row">
                    <div className="cell vehicles">{vehicleModel}</div> 
                  </div>
                )
            }
          </div>

          <div className="years-col">
            {/* Renders top list with years */}
            <div className="row">
            {
              state['years']
                .map((year, i) =>
                  <div key={i} className="cell years">
                      <span className="year-cell">{year}</span>
                  </div>
                )
            }
            </div>
            {/* Renders clickable cells */}
            {
              state['vehicle-models']
                .map((vehicleModel, i) => 

                <div 
                  key={i}
                  className="row">
                  {
                    state['years']
                      .map((year, j) => 
                        <ModelYearCell
                          key={j}
                          year={year}
                          handleToggleCovered={handleToggleCovered}
                          vehicleModel={VehicleModelsEnum[vehicleModel]}
                          modelCoverage={state.coverage[VehicleModelsEnum[vehicleModel]]}
                        />
                      )
                  }
                </div>

              )
            }
        </div>
      </div>
    </div>
  )
};

const logoStyles = {
  backgroundImage: `url(${logo})`,
};
