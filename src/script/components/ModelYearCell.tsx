import * as React from 'react';
import { useEffect, useState } from 'react';
import { CellModel } from '../models/CellModel';
import { VehicleModelsEnum } from '../models/CoverageModel';

/**
 * @param props 
 * @returns A component to handle the state of a vehicle-model->year cell and visually display 
 * it's coverage years state
 */
export const ModelYearCell: React.FC<CellModel> = props => {
  const [isYearCovered, setIsYearCovered] = useState(false);

  // Checks if the year on this cell is on the coverage array
  useEffect(() => {
    const isCovered = props.modelCoverage.find(year_ => props.year === year_);
    setIsYearCovered(typeof isCovered === 'number');
  }, [props.modelCoverage]);
  return (
    <div 
      className={`cell ${isYearCovered ? 'active' : ''}`}
      onClick={() => props.handleToggleCovered(VehicleModelsEnum[props.vehicleModel], props.year)}/>
  );

};
