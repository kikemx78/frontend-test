export enum VehicleModelsEnum {
  ILX = 'ILX',
  MDX = 'MDX',
  RDX = 'RDX',
  RLX = 'RLX',
  TL = 'TL',
  TLX = 'TLX',
  TSX = 'TSX',
}

export class CoverageModel {
  /**
   * A collection of vehicle models
   */
  "vehicle-models": string[] = [];
  /**
   * A collection of Years
   */
  "years": number[] = [];
  /**
   * Map vehicle-model->years of coverage
   */
  "coverage": {
    [key in VehicleModelsEnum]: number[]
  }; 
}