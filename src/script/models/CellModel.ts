import { VehicleModelsEnum } from "./CoverageModel";

export class CellModel {
  /**
   * this cells vehicle-model
   */
  vehicleModel: VehicleModelsEnum = VehicleModelsEnum.ILX;
  /**
   * this cells year
   */
  year: number = -1;
  /**
   * array including this vehicle-model coverage years
   */
  modelCoverage: number[] = [];
  /**
   * Adds/Removes this Year to/from coverage years arr
   */
  handleToggleCovered: (vehicleModel: VehicleModelsEnum, year: number) => void;
};